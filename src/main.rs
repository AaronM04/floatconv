use std::cmp;
use std::io;
use std::mem;
#[macro_use] extern crate anyhow;
use anyhow as A;
use A::Context;

const LOW_EXP2: isize = -1074;
const HIGH_EXP2: isize = 1023;

///Represents the maximum digits and maximum value that fit in a given integer type for a particular base
///10 packing.
#[derive(Clone, Copy, Debug)]
struct BaseInfo {
    digits: usize,
    base: usize,
}

impl BaseInfo {
    fn new() -> Self {
        let mut digits = 0;
        let mut base: usize = 1;
        while let Some(n) = base.checked_mul(10) {
            // ensure there are enough digits for common operations
            if n.checked_mul(5).and_then(|m| m.checked_add(5)).is_none() {
                break;
            }
            digits += 1;
            base = n;
        }
        BaseInfo{digits, base}
    }
}

/// This is the most incomplete big number implementation ever written. It only supports
/// initialization with a value of 1, multiplication by 2 or 5, and division by 10. Negative
/// integers are not supported.
///
/// The value representation is unusual -- each machine word represents a group of base 10 digits.
/// This is somewhat similar to BCD (Binary Coded Decimal).
#[derive(Clone, Debug)]
struct BigNum {
    base_info: BaseInfo,
    value: Vec<usize>, // The least significant word is at index 0
    exp10: isize, // The value needs to be multiplied by 10 raised to this power to get the true value of this BigNum
}

impl BigNum {
    pub fn one() -> Self {
        BigNum {
            base_info: BaseInfo::new(),
            value: vec![1],
            exp10: 0,
        }
    }

    pub fn mul2(&mut self) {
        for i in 0..self.value.len() {
            self.value[i] <<= 1;
        }
        self.do_carry();
    }

    pub fn mul5(&mut self) {
        for i in 0..self.value.len() {
            self.value[i] *= 5;
        }
        self.do_carry();
    }

    pub fn div10(&mut self) {
        self.exp10 -= 1;
    }

    fn do_carry(&mut self) {
        let mut carry = 0;
        for i in 0..self.value.len() {
            self.value[i] += carry;
            carry = self.value[i] / self.base_info.base;
            self.value[i] %= self.base_info.base;
        }
        if carry > 0 {
            self.value.push(carry);
        }
    }

    /// Gets the most significant piece.
    pub fn piece(&self) -> Piece {
        let mut exp10 = self.exp10 + (self.base_info.digits * (self.value.len() - 1)) as isize;
        let mut value = self.value[self.value.len()-1];
        let min = self.base_info.base / 10; // Value must be at least this to have most sig. base 10 digit nonzero.
        let mut need = 0;
        while value < min {
            value *= 10;
            exp10 -= 1;
            need += 1;
        }
        // Need at least one digit from the machine word less significant than the one value came
        // from.
        if need > 0 && self.value.len() > 1 {
            let mut low_value = self.value[self.value.len()-2];
            for _ in 0..(self.base_info.digits - need) {
                low_value /= 10;
            }
            value += low_value;
        }
        Piece{
            piece: value,
            exp10,
        }
    }
}

/// A Piece is like a single machine word in the BigNum values Vec except that its most significant
/// base 10 digit is not zero.
#[derive(Copy, Clone, Debug)]
struct Piece {
    piece: usize,
    exp10: isize, // The value needs to be multiplied by 10 raised to this power to get the true value
}

/// A Lookup represents the most significant N base 10 digits of all powers of 2 from 2**self.exp2 to
/// 2**(self.exp2 + pieces.len() - 1), inclusive, where N is self.base_info.digits.
#[derive(Clone, Debug)]
struct Lookup {
    base_info: BaseInfo,
    pieces: Vec<Piece>,
    exp2: isize,
}


impl Lookup {
    pub fn new() -> Self {
        let base_info = BaseInfo::new();
        let mut neg_vec = vec![]; // holds 2**-1 through 2**LOW_EXP2 in that order (descending)
        let mut bi = BigNum::one();
        for _exp2 in (LOW_EXP2..=-1).rev() {
            bi.mul5();
            bi.div10();
            neg_vec.push(bi.piece());
        }
        neg_vec.reverse();
        let mut pieces = neg_vec; // initially this is 2**LOW_EXP2 through 2**-1 in that order
        bi = BigNum::one();
        pieces.push(bi.piece());
        for _exp2 in 1..=HIGH_EXP2 {
            bi.mul2();
            pieces.push(bi.piece());
        }
        // now pieces is 2**LOW_EXP2 through 2**HIGH_EXP2 in that order
        Lookup{
            base_info,
            pieces,
            exp2: LOW_EXP2,
        }
    }

    /// Returns a vec of exp2 representing each 1 bit, or an error if a bit would be out of range.
    fn get_exp2s(&self, mut digits: usize, mut exp10: isize) -> A::Result<Vec<isize>> {
        if digits == 0 {
            // No 1 digits anywhere in a 0
            return Ok(vec![]);
        }

        // First normalize
        let min = self.base_info.base / 10; // Value must be at least this to have most sig. base 10 digit nonzero.
        while digits < min {
            digits *= 10;
            exp10 -= 1;
        }

        let mut exp2s = vec![];

        let high_exp2_idx = self.pieces.binary_search_by(|piece| {
            use cmp::Ordering;
            // Return Ordering::Greater if this piece is too large to be in the given number
            if piece.exp10 > exp10 || (piece.exp10 == exp10 && piece.piece > digits) {
                return Ordering::Greater;
            }
            // Special case that is essentially the case after it with everything multiplied by 10.
            if piece.exp10 == exp10 - 1 && piece.piece/5 > digits {
                return Ordering::Equal;
            }
            // Return Ordering::Less if this piece is <= half of the given number
            if piece.exp10 < exp10 || (piece.exp10 == exp10 && piece.piece <= (digits>>1)) {
                return Ordering::Less;
            }
            // Otherwise, return Ordering::Equal
            Ordering::Equal
        });
        if let Err(insert_exp2_idx) = high_exp2_idx {
            return Err(anyhow!("{}E{:04} is out of range (insert_exp2_idx is {})", digits, exp10, insert_exp2_idx));
        }
        let high_idx = high_exp2_idx.unwrap(); // unwrap OK because of above check
        let mut low_idx = high_idx as isize - 52;
        if low_idx < 0 {
            low_idx = 0;
        }
        for exp2_idx in ((low_idx as usize)..=high_idx).rev() {
            let mut piece = self.pieces[exp2_idx].piece;
            let mut piece_exp10 = self.pieces[exp2_idx].exp10;
            while piece_exp10 < exp10 {
                piece /= 10;
                piece_exp10 += 1;
            }
            if piece <= digits {
                digits -= piece;
                exp2s.push(exp2_idx as isize + self.exp2);
            }
            if digits == 0 {
                break;
            }
        }

        assert!(exp2s.len() > 0, "did not find any 1 bits"); // Bug!

        Ok(exp2s)
    }
}

fn convert_to_float(dec: &str) -> A::Result<f64> {
    let decimal_position = dec.find(".");
    let dec_before_dot = if let Some(pos) = decimal_position {
        &dec[0..pos]
    } else {
        dec
    };
    let before_dot: i64 = if dec_before_dot.len() > 0 {
        dec_before_dot.parse().context("invalid number part before decimal")?
    } else { 0 };
    let s: u64 = if before_dot < 0 { 1 } else { 0 };

    let exp2s = vec![]; //XXX get from Lookup::get_exp2s
    let (e, m) = e_m_from_exp2s(&exp2s)?;

    // s, e, m
    if s > 1 {
        return Err(anyhow!("Sign bit out of range: {}", s));
    }
    if e&0x7ff > 0 {
        return Err(anyhow!("e out of range: {}", e));
    }
    if m&0xfffffffffffff > 0 {
        return Err(anyhow!("mantissa out of range: 0x{:x}", m));
    }
    let raw = (s << 63) |
        (e << 52) |
        m;
    Ok(unsafe {
        mem::transmute::<u64, f64>(raw)
    })
}

/// When given a slice of base 2 exponents (with the most positive first), return an IEEE-754
/// double precision e and m value.
fn e_m_from_exp2s(exp2s: &[isize]) -> A::Result<(u64, u64)> {
    fn range_check(exp2: isize) -> A::Result<()> {
        if exp2 < LOW_EXP2 || exp2 > HIGH_EXP2 {
            return Err(anyhow!("exp2 out of range! {} not in range [{}, {}]", exp2, LOW_EXP2, HIGH_EXP2));
        }
        Ok(())
    };
    if exp2s.len() == 0 {
        return Ok((0, 0));
    }
    let high_exp2 = exp2s[0];
    range_check(high_exp2)?;
    let mut e = cmp::max(0, high_exp2+1023) as u64;
    let mut m = 0;
    let mut exp2s_iter = exp2s.iter();
    if high_exp2 >= -1022 {
        // Not subnormal
        exp2s_iter.next(); // Discard the highest because the highest 1 bit is implied in normal representation
    }
    // When high_exp2 is   100, m |= 1<<( exp2 - 48 ). e is 1123.
    // When high_exp2 is     0, m |= 1<<( exp2 + 52 ). e is 1023.
    // When high_exp2 is -1022, m |= 1<<( exp2+1074 ). e is    1.
    // When high_exp2 is -1023, m |= 1<<( exp2+1074 ). e is    0.
    // When high_exp2 is -1074, m |= 1<<( exp2+1074 ). e is    0.
    let offset;
    if high_exp2 > -1023 {
        offset = 52 - high_exp2;
    } else {
        offset = 1074;
    }
    for exp2 in exp2s_iter {
        m |= 1<<(exp2+offset);
    }
    Ok((e, m))
}

fn prompt_string(prompt: &str) -> A::Result<String> {
    println!("Enter {}: ", prompt);
    let mut line = String::new();
    io::stdin().read_line(&mut line)?;
    Ok(line.trim().to_owned())
}

fn prompt_usize(prompt: &str) -> A::Result<usize> {
    println!("Enter {}: ", prompt);
    let mut line = String::new();
    io::stdin().read_line(&mut line)?;
    Ok(line.trim().parse()?)
}

fn prompt_isize(prompt: &str) -> A::Result<isize> {
    println!("Enter {}: ", prompt);
    let mut line = String::new();
    io::stdin().read_line(&mut line)?;
    Ok(line.trim().parse()?)
}

fn main() -> A::Result<()> {
    let lookup = Lookup::new();
    let (digits, exp10) = (
        prompt_usize("digits")?,
        prompt_isize("exp10")?
    );
    let exp2s = lookup.get_exp2s(digits, exp10)?;
    println!("exp2s: {:?}", exp2s);

    let (e, m) = e_m_from_exp2s(&exp2s)?;
    println!("e: {}, m: 0x{:08x}", e, m);

    println!("transmuted: {:e}", unsafe{mem::transmute::<u64,f64>(
            ((e & 2047) << 52) |
            (m & ((1<<53)-1))
            )}); //XXX delete this

    //let dec = prompt_string("a number")?;
    //println!("converted to float: {:?}", convert_to_float(&dec)?);
    Ok(())
}
